import { Component, OnInit } from '@angular/core';
//import { ApiService } from '../../../services/restapi/api.service';
import { GetallhotelsService } from '../../../services/restapi/getallhotels.service';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {

  hotels = [];
  rowsPerPage = 5;
  SampleBinding = '';
  constructor(private getAllHotels: GetallhotelsService) { }

  ngOnInit() {
    this.getHotels();
  }

  getHotels(): void {
    this.getAllHotels.getAllHotels()
      .subscribe(hotels => this.hotels = hotels);
  }


}