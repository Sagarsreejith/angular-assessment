import { Component, OnInit } from '@angular/core';
import { GetgithubdetailsService } from '../../../services/restapi/getgithubdetails.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  gitHubUserName = '';

  userdetails = {};
  constructor(private apiService: GetgithubdetailsService) { }

  ngOnInit() {
  }

  submitUsername(username: string): void {
    this.apiService.getDetails(username)
      .subscribe(userdetails => this.userdetails = userdetails);
    }

}