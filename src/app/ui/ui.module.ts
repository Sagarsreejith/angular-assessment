import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';
import { DataTableModule } from "angular-6-datatable";
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DatatableComponent } from './common/datatable/datatable.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    DataTableModule,
  ],
  declarations: [LayoutComponent, HeaderComponent, FooterComponent, DatatableComponent, SidebarComponent],
  exports: [LayoutComponent]
})
export class UiModule { }
