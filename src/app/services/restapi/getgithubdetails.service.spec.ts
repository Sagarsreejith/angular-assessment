import { TestBed, inject } from '@angular/core/testing';

import { GetgithubdetailsService } from './getgithubdetails.service';

describe('GetgithubdetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetgithubdetailsService]
    });
  });

  it('should be created', inject([GetgithubdetailsService], (service: GetgithubdetailsService) => {
    expect(service).toBeTruthy();
  }));
});
