import { TestBed, inject } from '@angular/core/testing';

import { GetallhotelsService } from './getallhotels.service';

describe('GetallhotelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetallhotelsService]
    });
  });

  it('should be created', inject([GetallhotelsService], (service: GetallhotelsService) => {
    expect(service).toBeTruthy();
  }));
});
