import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Data } from '../../data';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { MessageService } from '../common/message.service';

//if we want to pass headers pass this
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GetallhotelsService {
  private apiUrl = 'http://localhost:8090/example/v1/hotels?page=0&size=10';
  constructor(private http: HttpClient, private messageService: MessageService) { }

  /** GET all Hotesl from the server */
  getAllHotels(): Observable<Data[]> {
    return this.http.get<Data[]>(this.apiUrl, httpOptions)
      .pipe(
        tap(data => this.log('fetched datas')),
        catchError(this.handleError('getdata', []))
      );
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`dataService: ${message}`);
  }
}
