export class Data {
    position: number;
    name: string;
    weight: number;
    symbol: string;
}